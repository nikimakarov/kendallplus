﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Kendall
{
    public class KendallPURrMethod
    {
        double limit = 0;

        /*public Dictionary<int, int> getRanks(Dictionary<Int32, Double> result)
        {
            List<double> values = result.Select(r => r.Value).ToList();
            List<int> keys = result.Select(r => r.Key).ToList();

            int rank = 1;
            Dictionary<int, int> ranks = new Dictionary<int, int>(10);
            for (int i = 0; i < values.Count(); i++)
            {
                if (i != 0 && values[i - 1] != values[i])
                {
                    rank++;
                }
                ranks.Add(keys[i] + 1, rank);
            }

            return ranks;
        }*/

        public void process()
        {
            bool consistency = false;
            double concordance = 0;


            //TODO: Check PURr for the same positions in getResult(), because rangs always different

            /*
            MethodsData data = new MethodsData(
            expertsNames: new List<String> { "Expert 1", "Expert 2", "Expert 3"},
            situationsNames: new List<String> { "Stable", "Crisis" },
            critertiasNames: new List<String> { "Monthly income", "Account balance", "Term",
                "Number of dependents", "Loan purpose", "Age", "Profession"},
            alternativesNames: new List<String> { "Loaner 1", "Loaner 2", "Loaner 3", "Loaner 4" },
            expertCoefficients: new List<Double> { 0.4, 0.3, 0.3 },
            situationProbabilities: new List<Double> { 0.8, 0.2 },
            criteriaValues: new List<Double> { 0.15, 0.2, 0.15, 0.15, 0.15, 0.1, 0.1 }); */

            MethodsData data = new MethodsData(
            expertsNames: new List<String> { "Expert 1", "Expert 2", "Expert 3" },
            situationsNames: new List<String> { "Stable", "Crisis" },
            critertiasNames: new List<String> { "Interest Rate", "Value", "Term", "Appointment", "Complexity" },
            alternativesNames: new List<String> { "Credit <Happy Year>", "Credit <Small Percent>",
                "Loaner <Big Spot>", "Credit <Eazy Way>" },
            expertCoefficients: new List<Double> { 0.4, 0.3, 0.3 },
            situationProbabilities: new List<Double> { 0.8, 0.2 },
            criteriaValues: new List<Double> { 0.15, 0.2, 0.15, 0.2, 0.3 });
            Console.WriteLine("Please enter the consistency limit:");
            limit = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();
            do
            {
               
               int[,,,] example = new int[,,,]{
                   { // expert 1
                       { {2, 3, 3, 2, 2}, {1, 2, 1, 1, 2}, {3, 3, 1, 2, 1}, {3, 4, 4, 1, 1}}, // situation 1
                       { {1, 3, 2, 2, 3}, {1, 2, 1, 1, 3}, {2, 1, 2, 2, 2}, {2, 4, 3, 1, 1}}, // situation 2
                   },
                   { // expert 2
                       { {1, 3, 2, 4, 4}, {1, 1, 1, 3, 3}, {3, 3, 3, 4, 1}, {2, 2, 4, 2, 2}}, // situation 1
                       { {1, 2, 2, 1, 3}, {1, 2, 1, 3, 4}, {2, 1, 3, 4, 1}, {4, 3, 3, 2, 2}}, // situation 2
                   },
                   { // expert 3
                       { {1, 3, 2, 3, 4}, {2, 1, 1, 2, 2}, {2, 2, 3, 3, 1}, {2, 3, 2, 2, 3}}, // situation 1
                       { {1, 3, 1, 1, 3}, {2, 2, 1, 3, 4}, {3, 1, 2, 4, 1}, {4, 3, 1, 2, 2}}, // situation 2
                   }
               };

                /*
                int[,,,] example = new int[,,,]{
                    { // expert 1
                        { {2, 1, 3, 2, 2, 2, 2}, {1, 2, 2, 1, 2, 2, 1}, {3, 3, 1, 2, 1, 1, 1}, {3, 4, 4, 1, 1, 3, 3}}, // situation 1
                        { {1, 3, 2, 2, 3, 3, 3}, {1, 2, 1, 1, 3, 2, 4}, {2, 1, 2, 2, 2, 1, 1}, {2, 4, 3, 1, 1, 4, 2}}, // situation 2
                    },
                    { // expert 2
                        { {1, 1, 2, 1, 4, 3, 4}, {2, 2, 1, 3, 3, 2, 1}, {3, 3, 3, 4, 1, 1, 2}, {4, 4, 4, 2, 2, 4, 3}}, // situation 1
                        { {1, 2, 2, 1, 3, 3, 4}, {3, 3, 1, 3, 4, 1, 2}, {2, 1, 3, 4, 1, 2, 3}, {4, 4, 4, 2, 2, 4, 1}}, // situation 2
                    },
                    { // expert 3
                        { {1, 3, 2, 1, 4, 2, 3}, {2, 1, 1, 2, 2, 2, 1}, {2, 2, 3, 3, 1, 1, 2}, {2, 3, 2, 2, 3, 3, 3}}, // situation 1
                        { {1, 3, 1, 1, 3, 2, 2}, {2, 2, 1, 3, 4, 3, 1}, {3, 1, 2, 4, 1, 1, 3}, {4, 3, 1, 2, 2, 3, 3}}, // situation 2
                    }
                };*/

                data.setExpertScores(example);

                /*Console.WriteLine("Please enter the experts ratings:");
                int[,,,] inputExpertsRates = new int[data.getExpertsCount(), data.getSituationsCount(), data.getAlternativesCount(), data.getCriteriaCount()];
                for (int e = 0; e < data.getExpertsCount(); e++)
                {
                    Console.WriteLine("Expert {0}", data.getExpertsNames()[e]);
                    for (int s = 0; s < data.getSituationsCount(); s++)
                    {
                        Console.WriteLine("Situation {0}", data.getSituationsNames()[s]);
                        for (int c = 0; c < data.getCriteriaCount(); c++)
                        {
                            Console.WriteLine("Criteria {0}", data.getCriteriasNames()[c]);
                            for (int al = 0; al < data.getAlternativesCount(); al++)
                            {
                                Console.WriteLine("Alternative {0}", data.getAlternativesNames()[al]);
                                inputExpertsRates[e, s, al, c] = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine();
                            }
                        }
                    }
                           
                }

                data.setExpertScores(inputExpertsRates); */

                //PURr method for every expert separately
                List<Dictionary<Int32, Int32>> expertsAlternatives = new List<Dictionary<Int32, Int32>>();
                PURr PURrMethod = new PURr(data);
                for (int i = 0; i < data.getExpertsCount(); i++)
                {
                    expertsAlternatives.Add(PURrMethod.processOneExpert(i));
                }
                getPURrSortedResults(expertsAlternatives, data);
                int[,] scores = getPURrResults(expertsAlternatives, data);

                // Find Kendall Concordance Coefficient
                KendallAlternativeAndExpert KendallMethod = new KendallAlternativeAndExpert(data, scores, limit);
                KendallMethod.process();
                concordance = KendallMethod.getConcordance();
                consistency = KendallMethod.getConsistencyStatus();
                Console.WriteLine("Concordance: {0}", concordance);
                Console.WriteLine("Consistency Status: {0}", consistency);
                if (consistency)
                {
                    Console.WriteLine();
                    Console.WriteLine("Final Results");
                    Console.WriteLine("Final Concordance: {0}", concordance);

                    PURr PURrMethodFull = new PURr(data);
                    List<Double> res = PURrMethodFull.processAll();
                    for (int i = 0; i < data.getAlternativesCount(); i++)
                    {
                        Console.WriteLine("Alternative score of {0} is {1}", data.getAlternativesNames()[i], res[i]);
                    }
                }
                else
                {
                    Console.WriteLine("Continue and change ratings or exit (c/e)?");
                    String answer = Console.ReadLine();
                    if (answer == "e")
                    {
                        consistency = true;
                    }
                }
            } while (!consistency);
        }

        private static int[,] getPURrSortedResults(IEnumerable<Dictionary<Int32, Int32>> list, MethodsData data)
        {
            int[,] scores =  new int[data.getExpertsCount(), data.getAlternativesCount()];
            int expertIndex = 0;
            foreach (Dictionary<Int32, Int32> expertList in list)
            {
                Console.WriteLine("Expert: {0}", data.getExpertsNames()[expertIndex]);
                int altIndex = 0;
                foreach (KeyValuePair<int,int> pair in expertList)
                {
                    Console.WriteLine("Alternative: {0} - {1}", data.getAlternativesNames()[pair.Key], pair.Value);
                    scores[expertIndex, altIndex] = pair.Value;
                    altIndex++;
                }
                Console.WriteLine();
                expertIndex++;
            }
            return scores;
        }

        private static int[,] getPURrResults(IEnumerable<Dictionary<Int32, Int32>> list, MethodsData data)
        {
            int[,] scores = new int[data.getExpertsCount(), data.getAlternativesCount()];
            int expertIndex = 0;
            foreach (Dictionary<Int32, Int32> expertList in list)
            {
                int altIndex = 0;
                foreach (KeyValuePair<int, int> pair in expertList.OrderBy(key => key.Key))
                {
                    scores[expertIndex, altIndex] = pair.Value;
                    altIndex++;
                }
                expertIndex++;
            }
            return scores;
        }

    }

    internal class List<T1, T2>
    {
    }
}
