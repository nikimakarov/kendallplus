﻿using System;
namespace Kendall
{
    public class Kendall
    {
        private double ranksSumVariance; // Дисперсия суммы рангов
        private double mathExpectation; // Оценка математического ожидания
        private double[,] concordance; // Матрица с коэффициентами конкордации Кендалла
        private bool[,] consistency; // Матрица согласованности
        private double consistencyLimit; // Ограничение согласованности
        private bool consistenyStatus;

        private int expertsCount;
        private int situationsCount;
        private int alternativesCount;
        private int criteriaCount;
        private int[,,,] expertScores;

        public Kendall(MethodsData data, double limit)
        {
            expertsCount = data.getExpertsCount();
            situationsCount = data.getSituationsCount();
            alternativesCount = data.getAlternativesCount();
            criteriaCount = data.getCriteriaCount();
            expertScores = (int[,,,])data.getExpertScores().Clone();

            ranksSumVariance = 0;
            consistenyStatus = false;
            consistencyLimit = limit;
            mathExpectation = expertsCount * (alternativesCount + 1) / 2;
            consistency = new bool[criteriaCount, situationsCount];
            concordance = new double[criteriaCount, situationsCount];
        }

        private void makeConcordanceMatrix()
        {
            for(int s = 0; s < situationsCount; s++) // По всем ситуациям
            {
                for (int c = 0; c < criteriaCount; c++) //По всем признакам
                {
                    ranksSumVariance = 0;
                    for (int alt = 0; alt < alternativesCount; alt++) // По всем альтернативам
                    {
                        int srang = 0; // Сумма рангов
                        for (int e = 0; e < expertsCount; e++)
                        {
                            srang = srang + expertScores[e, s, alt, c];
                        }
                        ranksSumVariance = ranksSumVariance + (srang - mathExpectation) * (srang - mathExpectation); // Вычисление дисперсии суммы рангов
                    }
                    concordance[c, s] = 12 * ranksSumVariance / (expertsCount * expertsCount * (alternativesCount
                            * alternativesCount * alternativesCount - alternativesCount)); // Расчет коэффициента конкордации
                }
            }
        }

        private void makeConsistencyMatrix()
        {
            consistenyStatus = true;
            for (int s = 0; s < situationsCount; s++) // По всем ситуациям
            {
                for (int c = 0; c < criteriaCount; c++) // По всем признакам
                {
                    consistency[c, s] = (concordance[c, s] <= consistencyLimit);
                    if (consistency[c, s])
                    {
                        consistenyStatus = false;
                    }
                }
            }
        }

        public void process()
        {
            makeConcordanceMatrix();
            makeConsistencyMatrix();
        }

        public bool[,] getConsistency()
        {
            return consistency;
        }

        public double[,] getConcordance()
        {
            return concordance;
        }

        public bool getConsistencyStatus()
        {
            return consistenyStatus;
        }
    }
}