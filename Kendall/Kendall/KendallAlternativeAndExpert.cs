﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kendall
{
    public class KendallAlternativeAndExpert
    {
        private double ranksSumVariance; // Дисперсия суммы рангов
        private double mathExpectation; // Оценка математического ожидания
        private double concordance; // Матрица с коэффициентами конкордации Кендалла
        private double concordance_2;
        private double consistencyLimit; // Ограничение согласованности
        private bool consistenyStatus;

        private int[,] expertAlternativeScores;
        private int expertsCount;
        private int alternativesCount;
        protected List<String> alternativesNames;

        public KendallAlternativeAndExpert(MethodsData data, int[,] scores, double limit)
        {
            alternativesNames = data.getAlternativesNames();
            expertsCount = data.getExpertsCount();
            alternativesCount = data.getAlternativesCount();
            expertAlternativeScores = (int[,])scores.Clone();
            ranksSumVariance = 0;
            consistenyStatus = false;
            consistencyLimit = limit;
            mathExpectation = expertsCount * (alternativesCount + 1) / 2;
            concordance = 0;
        }

        // Connected ranks indicator function for concordance coefficient
        private Tuple<double[], int> getConnectedRanksIndicator()
        {
            double[] connectedRanksIndicators = new double[expertsCount];
            int expertsWithConnectedRanks = 0;
            for (int e = 0; e < expertsCount; e++)
            {
                int connectedRanksExpertIndicator = 0;
                int[] expertScores = new int[alternativesCount];
                for (int alt = 0; alt < alternativesCount; alt++)
                {
                    expertScores[alt] = expertAlternativeScores[e, alt];
                }
                bool hasConnectedRanks = false;
                foreach (int val in expertScores.Distinct())
                {
                    int count = expertScores.Count(x => x == val);
                    if (count > 1)
                    {
                        connectedRanksExpertIndicator += (count * count * count) - count;
                        hasConnectedRanks = true;
                    }
                }
                connectedRanksIndicators[e] = connectedRanksExpertIndicator;
                if (hasConnectedRanks)
                {
                    expertsWithConnectedRanks++;
                }
            }
            return new Tuple<double[],int>(connectedRanksIndicators, expertsWithConnectedRanks);
        }

        private double getConnectedRanks(double[] connectedRanksIndicators)
        {
            double sum = 0;
            for (int e = 0; e < expertsCount; e++)
            {
                sum += connectedRanksIndicators[e];
            }
            return sum;
        }

        // Connected ranks indicator function for correlation coefficients
        private double[] getConnectedRanksCorrelationIndicators()
        {
            double[] connectedRanksIndicators = new double[expertsCount];
            int expertsWithConnectedRanks = 0;
            for (int e = 0; e < expertsCount; e++)
            {
                int connectedRanksExpertIndicator = 0;
                int[] expertScores = new int[alternativesCount];
                for (int alt = 0; alt < alternativesCount; alt++)
                {
                    expertScores[alt] = expertAlternativeScores[e, alt];
                }

                bool hasConnectedRanks = false;
                foreach (int val in expertScores.Distinct())
                {
                    int count = expertScores.Count(x => x == val);
                    if (count > 1)
                    {
                        connectedRanksExpertIndicator += (count * count) - count;
                        hasConnectedRanks = true;
                    }
                }
                connectedRanksIndicators[e] = 0.5 * connectedRanksExpertIndicator;
                if (hasConnectedRanks)
                {
                    expertsWithConnectedRanks++;
                }
            }
            return connectedRanksIndicators;
        }

        class ExpertsRankCoefficients
        {
            public int expert1;
            public int expert2;
            public double value;

            public ExpertsRankCoefficients(int expert1, int expert2, double value)
            {
                this.expert1 = expert1;
                this.expert2 = expert2;
                this.value = value;
            }

            public override String ToString()
            {
                return "Ranks coefficient between Expert " + (this.expert1 + 1) +
                    " and Expert " + (this.expert2 + 1) + " equals " + this.value;
            }

        }

        // Get Expert Ranks Kendall Correlation
        private ExpertsRankCoefficients[] getRankKendallCorrelation()
        {
            int pairsCount = expertsCount * (expertsCount - 1) / 2;
            ExpertsRankCoefficients[] expertsKendallCorrelation = new ExpertsRankCoefficients[pairsCount];
            double[] connectedRanksIndicators = getConnectedRanksCorrelationIndicators();
            int index = 0;
            for (int e1 = 0; e1 < expertsCount; e1++)
            {
                for (int e2 = e1 + 1; e2 < expertsCount; e2++)
                {
                    double expertsSum = 0;
                    for (int i = 0; i < alternativesCount; i++)
                    {
                        for (int j = i + 1; j < alternativesCount; j++)
                        {
                            int expRank1 = expertAlternativeScores[e1, i] < expertAlternativeScores[e1, j] ? 1
                                : (expertAlternativeScores[e1, i] == expertAlternativeScores[e1, j] ? 0 : -1);
                            int expRank2 = expertAlternativeScores[e2, i] < expertAlternativeScores[e2, j] ? 1
                                : (expertAlternativeScores[e2, i] == expertAlternativeScores[e2, j] ? 0 : -1);
                            expertsSum += expRank1 * expRank2;
                        }
                    }

                    double expCount1 = (0.5 * alternativesCount * (alternativesCount - 1)) - connectedRanksIndicators[e1];
                    double expCount2 = (0.5 * alternativesCount * (alternativesCount - 1)) - connectedRanksIndicators[e2];
                    ExpertsRankCoefficients pair = new ExpertsRankCoefficients(e1, e2,
                        expertsSum / Math.Sqrt(expCount1 * expCount2));
                    expertsKendallCorrelation[index] = pair;
                    index++;
                }
            }
            return expertsKendallCorrelation;
        }

        // Get Expert Ranks Spearman Correlation
        private ExpertsRankCoefficients[] getRankSpearmanCorrelation()
        {
            int pairsCount = expertsCount * (expertsCount - 1) / 2;
            ExpertsRankCoefficients[] expertSpearmanCorrelation = new ExpertsRankCoefficients[pairsCount];
            double[] connectedRanksIndicators = getConnectedRanksCorrelationIndicators();
            int index = 0;
            for (int e1 = 0; e1 < expertsCount; e1++)
            {
                for (int e2 = e1 + 1; e2 < expertsCount; e2++)
                {
                    double ranksDiff = 0;
                    for (int i = 0; i < alternativesCount; i++)
                    {
                        ranksDiff += Math.Pow(expertAlternativeScores[e1, i] - expertAlternativeScores[e2, i], 2);
                    }
                    double alternativesTerm = (alternativesCount * (Math.Pow(alternativesCount, 2) - 1) / 6);
                    double expCount1 = alternativesTerm - 2 * connectedRanksIndicators[e1];
                    double expCount2 = alternativesTerm - 2 * connectedRanksIndicators[e2];
                    double numeratorTerm = alternativesTerm - (connectedRanksIndicators[e1] + connectedRanksIndicators[e2]);
                    ExpertsRankCoefficients pair = new ExpertsRankCoefficients(e1, e2, (numeratorTerm - ranksDiff) / Math.Sqrt(expCount1 * expCount2));
                    expertSpearmanCorrelation[index] = pair;
                    index++;
                }
            }
            return expertSpearmanCorrelation;
        }

        // Get Expert Ranks Pair Correlation
        private ExpertsRankCoefficients[] getRankPairCorrelation()
        {
            int pairsCount = expertsCount * (expertsCount - 1) / 2;
            ExpertsRankCoefficients[] expertPairCorrelation = new ExpertsRankCoefficients[pairsCount];
            double[] connectedRanksIndicators = getConnectedRanksIndicator().Item1;
            int index = 0;
            for (int e1 = 0; e1 < expertsCount; e1++)
            {
                for (int e2 = e1 + 1; e2 < expertsCount; e2++)
                {
                    double ranksDiff = 0;
                    for (int i = 0; i < alternativesCount; i++)
                    {
                        ranksDiff += Math.Pow(expertAlternativeScores[e1, i] - expertAlternativeScores[e2, i], 2);
                    }
                    double connectedRanksSum = connectedRanksIndicators[e1] + connectedRanksIndicators[e2];
                    double alternativesTerm = (alternativesCount * (Math.Pow(alternativesCount, 2) - 1) / 6);

                    ExpertsRankCoefficients pair = new ExpertsRankCoefficients(e1, e2,
                        1 - (ranksDiff / (alternativesTerm - connectedRanksSum / 12)));
                    expertPairCorrelation[index]= pair;
                    index++;
                }
            }
            return expertPairCorrelation;
        }

        class AlternativesRankCoefficients
        {
            public int alternative;
            public String alternativeName;
            public double value;

            public AlternativesRankCoefficients(int alternative, String alternativeName, double value)
            {
                this.alternative = alternative;
                this.alternativeName = alternativeName;
                this.value = value;
            }

            public override String ToString()
            {
                return "Ranks coefficient of Alternative " + alternativeName + " equals " + value;
            }

        }

        // Get Alternatives Rank Variation
        private AlternativesRankCoefficients[] getRankVariation()
        {
            AlternativesRankCoefficients[] alternativeRankCoefficients = new AlternativesRankCoefficients[alternativesCount];
            
            for (int alt = 0; alt < alternativesCount; alt++)
            {
                int[] alternativeScores = new int[expertsCount];
                double l = 0, sum = 0;
                for (int e = 0; e < expertsCount; e++)
                {
                    alternativeScores[e] = expertAlternativeScores[e, alt];
                }
                foreach (int val in alternativeScores.Distinct())
                {
                    int count = alternativeScores.Count(x => x == val);
                    if (count > 0)
                    {
                        l += 1;
                        sum += count * count;
                    }
                }
                double value = (l == 1) ? 0 : ((l / (l - 1)) * (1 - (sum / (expertsCount * expertsCount))));
                alternativeRankCoefficients[alt] = new AlternativesRankCoefficients(alt, alternativesNames[alt], value);
            }
            return alternativeRankCoefficients;
        }

        // Get Alternatives Becker Variation
        private AlternativesRankCoefficients[] getBeckerVariation()
        {
            AlternativesRankCoefficients[] alternativeBeckerCoefficients = new AlternativesRankCoefficients[alternativesCount];
            for (int alt = 0; alt < alternativesCount; alt++)
            {
                int[] alternativeScores = new int[expertsCount];
                double sum = 0;
                for (int e = 0; e < expertsCount; e++)
                {
                    alternativeScores[e] = expertAlternativeScores[e, alt];
                }

                List<KeyValuePair<int, int>> ranksCountList = new List<KeyValuePair<int, int>>();
                foreach (int val in alternativeScores.Distinct())
                {
                    int count = alternativeScores.Count(x => x == val);
                    ranksCountList.Add(new KeyValuePair<int, int>(val, count));
                }

                for (int j = 0; j < ranksCountList.Count(); j++)
                {
                    for (int k = j+1; k < ranksCountList.Count(); k++)
                    {
                        sum += ranksCountList[k].Value * ranksCountList[j].Value * (ranksCountList[k].Key - ranksCountList[j].Key);
                    }
                }
                double value = sum / (expertsCount * ((double)expertsCount - 1) / 2);
                alternativeBeckerCoefficients[alt] = new AlternativesRankCoefficients(alt, alternativesNames[alt], value);
            }
            return alternativeBeckerCoefficients;
        }

        // Write coefficients data to console (ExpertsRankCoefficients[])
        private void writeCoefficientData(String name, ExpertsRankCoefficients[] arr)
        {
            Console.WriteLine();
            Console.WriteLine(name);

            arr = arr.OrderBy(x => x.value).ToArray();

            foreach (ExpertsRankCoefficients item in arr)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();

            Console.WriteLine("Minimal values:");

            double min = arr.Min(x => x.value);
            ExpertsRankCoefficients[] arrMin = arr.Where(x => x.value == min).ToArray();

            foreach (ExpertsRankCoefficients item in arrMin)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
        }

        // Write coefficients data to console (AlternativesRankCoefficients[])
        private void writeCoefficientData(String name, AlternativesRankCoefficients[] arr)
        {
            Console.WriteLine();
            Console.WriteLine(name);

            arr = arr.OrderBy(x => x.value).ToArray();

            foreach (AlternativesRankCoefficients item in arr)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();

            Console.WriteLine("Maximal values:");

            double max = arr.Max(x => x.value);
            AlternativesRankCoefficients[] arrMin = arr.Where(x => x.value == max).ToArray();

            foreach (AlternativesRankCoefficients item in arrMin)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
        }

        // Launch Correlation Functions
        private void makeCorrelationCoefficients()
        {
            ExpertsRankCoefficients[] kendallCorellation = getRankKendallCorrelation();
            writeCoefficientData("1. Kendall Correlation", kendallCorellation);

            ExpertsRankCoefficients[] spearmanCorellation = getRankSpearmanCorrelation();
            writeCoefficientData("2. Spearman Correlation", spearmanCorellation);

            ExpertsRankCoefficients[] pairCorellation = getRankPairCorrelation();
            writeCoefficientData("3. Pair Correlation", pairCorellation);
        }

        // Launch Variation Functions
        private void makeVariationCoefficients()
        {
            AlternativesRankCoefficients[] rankVariation = getRankVariation();
            writeCoefficientData("1. Rank Variation", rankVariation);

            AlternativesRankCoefficients[] beckerVariation = getBeckerVariation();
            writeCoefficientData("2. Becker Variation", beckerVariation);
        }

        // Launch Concordance Function
        private void makeConcordanceMatrix()
        {
            ranksSumVariance = 0;
            // По всем альтернативам
            for (int alt = 0; alt < alternativesCount; alt++)
            {
                int srang = 0; // Сумма рангов
                for (int e = 0; e < expertsCount; e++)
                {
                    srang = srang + expertAlternativeScores[e, alt];
                }
                // Вычисление дисперсии суммы рангов
                ranksSumVariance = ranksSumVariance + (srang - mathExpectation) * (srang - mathExpectation);
            }

            Tuple< double[], int> indicators = getConnectedRanksIndicator();
            double connectedRanksIndicator = getConnectedRanks(indicators.Item1);
            int expertsWithConnectedRanks = indicators.Item2;

            // Расчет коэффициента конкордации
            concordance_2 = 12 * ranksSumVariance / (expertsCount * expertsCount * (alternativesCount
                    * alternativesCount * alternativesCount - alternativesCount) - expertsCount * connectedRanksIndicator);

            // Расчет коэффициента конкордации (от авторов [2])
            concordance = 12 * ranksSumVariance / (expertsCount * expertsCount * (alternativesCount
                    * alternativesCount * alternativesCount - alternativesCount) -
                    (2 * expertsCount - expertsWithConnectedRanks) * connectedRanksIndicator);

            Console.WriteLine("Connected ranks indicator = " + connectedRanksIndicator);
            Console.WriteLine("Concordance v1 = " + concordance_2);
            Console.WriteLine("Concordance v2 = " + concordance);
        }

        private void checkConsistency()
        {
            consistenyStatus = true;
            if (concordance < consistencyLimit)
            {
                consistenyStatus = false;
            }
        }

        public void process()
        {
            makeCorrelationCoefficients();
            makeVariationCoefficients();
            makeConcordanceMatrix();
            checkConsistency();
        }

        public double getConcordance()
        {
            return concordance;
        }

        public bool getConsistencyStatus()
        {
            return consistenyStatus;
        }
    }
}
