﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNumerics.LinearAlgebra;
using ExpertSystem.Edss.Math;
using ExpertSystem.Edss.Methods.Tools;

namespace ExpertSystem.Edss.Methods
{
    /// <summary>
    /// Метод Kendall
    /// </summary>
    [ExpertMethod("KendallPURr")]
    public class KendallPuRr : PURqMethod
    {
        protected override void SolveInternal(StringBuilder resultHtml)
        {
            // TODO: Получить величину ограничения согласованности из интерфейса
            Double consistencyLimit = 0;

            Vector expertCoefficients = GetData<Vector>(DataInstanceType.ExpertCompetenceScore).NormalizeLinear();
            Vector situationProbabilities = GetData<Vector>(DataInstanceType.SituationProbabilities).NormalizeLinear();
            Vector criteriaValues = GetData<Vector>(DataInstanceType.CriterionRelativeValues).NormalizeLinear();
            Tesseract expertScores = GetData<Tesseract>(DataInstanceType.ExpertScores);

            List<MethodChildEntity> alternatives = GetChildEntitiesList(EntityType.Alternatives);

            int expertsCount = expertCoefficients.Length,
                situationsCount = situationProbabilities.Length,
                alternativesCount = alternatives.Count,
                criteriaCount = criteriaValues.Length;

            //
            // [ 1.PURr | Метод большинства для каждого эксперта в отдельности ]
            //

            // Обобщенная матрица парных сравнений
            double[,] generalPairCompareMatrix = new double[alternativesCount, alternativesCount];

            Dictionary<Int32, Double> expertRates = new Dictionary<Int32, Double>(capacity: alternativesCount);
            List<List<int>> expertsAlternatives = new List<List<int>>();

            // Для каждого из экспертов применить метод большинства
            for (int e = 0; e < expertsCount; e++)
            {
                // Построение обобщенной матрицы парных сравнений
                double ke = 1; // Коэффициент компетентности эксперта = 1, так как один эксперт

                for (int s = 0; s < situationsCount; s++)
                {
                    double ks = situationProbabilities[s]; // Вероятность ситуации

                    for (int c = 0; c < criteriaCount; c++)
                    {
                        double kc = criteriaValues[c]; // Коэффициент значимости признака

                        for (int a1 = 0; a1 < alternativesCount; a1++)
                        {
                            for (int a2 = 0; a2 < alternativesCount; a2++)
                            {
                                if (expertScores[e, s, a1, c] <= expertScores[e, s, a2, c])
                                {
                                    generalPairCompareMatrix[a1, a2] += kc * ks * ke;
                                }
                            }
                        }
                    }
                }

                // Построение медианной матрицы: заполнение generalPairCompareMatrix булевскими значениями
                double sumAlternativesScore = 0;
                for (int a1 = 0; a1 < alternativesCount; a1++)
                {
                    for (int a2 = 0; a2 < alternativesCount; a2++)
                    {
                        double t = generalPairCompareMatrix[a1, a2] >= 0.5 ? 1 : 0;
                        generalPairCompareMatrix[a1, a2] = t;
                        sumAlternativesScore += t;
                    }
                }

                // Получение коэффициентов вариантов решения
                for (int a1 = 0; a1 < alternativesCount; a1++)
                {
                    double rowSum = 0;
                    for (int a2 = 0; a2 < alternativesCount; a2++)
                    {
                        rowSum += generalPairCompareMatrix[a1, a2];
                    }
                    expertRates.Add(a1, rowSum / sumAlternativesScore);
                }

                // Получение порядковых величин для каждой альтернативы
                List<int> expertSerialRates = new List<int>();
                foreach (KeyValuePair<Int32, Double> number in expertRates.OrderBy(key => key.Value))
                {
                    expertSerialRates.Add(number.Key + 1);
                }

                expertsAlternatives.Add(expertSerialRates);
                expertRates.Clear();
            }

            //
            // [ 2.Kendall | Расчет коэффициента конкордации Кендалла]
            //

            double ranksSumVariance = 0; // Дисперсия суммы рангов
            double concordance; // Коэффициент конкордации Кендалла
            double mathExpectation = expertsCount * (alternativesCount + 1) / 2; // Оценка математического ожидания

            for (int alt = 0; alt < alternativesCount; alt++) // По всем альтернативам
            {
                double srang = 0; // Сумма рангов
                for (int e = 0; e < expertsCount; e++)
                {
                    srang = srang + expertsAlternatives[e][alt];
                }
                ranksSumVariance = ranksSumVariance + (srang - mathExpectation) * (srang - mathExpectation); // Вычисление дисперсии суммы рангов
            }
            concordance = 12 * ranksSumVariance / (expertsCount * expertsCount * (alternativesCount
                    * alternativesCount * alternativesCount - alternativesCount)); // Расчет коэффициента конкордации

            if (concordance >= consistencyLimit)
            {
                //
                // [ 3.PURr | Стандартный метод большинства]
                //

                // Построение обобщенной матрицы парных сравнений
                generalPairCompareMatrix = new double[alternativesCount, alternativesCount];
                for (int e = 0; e < expertsCount; e++)
                {
                    double ke = expertCoefficients[e]; // Коэффициент компетентности эксперта

                    for (int s = 0; s < situationsCount; s++)
                    {
                        double ks = situationProbabilities[s]; // Вероятность ситуации

                        for (int c = 0; c < criteriaCount; c++)
                        {
                            double kc = criteriaValues[c]; // Коэффициент значимости признака

                            for (int a1 = 0; a1 < alternativesCount; a1++)
                                for (int a2 = 0; a2 < alternativesCount; a2++)
                                    if (expertScores[e, s, a1, c] <= expertScores[e, s, a2, c])
                                        generalPairCompareMatrix[a1, a2] += kc * ks * ke;
                        }
                    }
                }

                // Построение медианной матрицы: заполнение generalPairCompareMatrix булевскими значениями
                double sum = 0;
                for (int a1 = 0; a1 < alternativesCount; a1++)
                    for (int a2 = 0; a2 < alternativesCount; a2++)
                    {
                        double t = generalPairCompareMatrix[a1, a2] >= 0.5 ? 1 : 0;
                        generalPairCompareMatrix[a1, a2] = t;
                        sum += t;
                    }

                // Получение коэффициентов вариантов решения
                Vector result = new Vector(alternatives.Count);
                for (int a1 = 0; a1 < alternativesCount; a1++)
                {
                    double rowSum = 0;
                    for (int a2 = 0; a2 < alternativesCount; a2++)
                        rowSum += generalPairCompareMatrix[a1, a2];

                    result[a1] = rowSum / sum;
                }

                ResultFormatter.FormatRankResult(resultHtml, result.ToArray(), alternatives);
            }
            else
            {
                // TODO: Вывести сообщение: "Коэффициент конкордации равен {concordance}, что меньше заданного ограничения {consistencyLimit}. Запросите новые оценки."
            }
        }
    }

}
*/