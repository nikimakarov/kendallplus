﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kendall
{
    public class PURr
    {
        private double sumAlternativesScore = 0;

        private List<Double> expertCoefficients;
        private List<Double> situationProbabilities;
        private List<Double> criteriaValues;
        private int[,,,] expertScores;

        private int expertsCount;
        private int situationsCount;
        private int criteriaCount;
        private int alternativesCount;

        private double[,] generalPairCompareMatrix;

        //private List<Double> result;
        private Dictionary<Int32, Double> result;
        private List<Double> resultFull;

        // Input values initialization
        public PURr(MethodsData data)
        {
            expertCoefficients = new List<Double>(data.getExpertCoefficients());
            situationProbabilities = new List<Double>(data.getSituationProbabilities());
            criteriaValues = new List<Double>(data.getCriteriaValues());

            expertsCount = data.getExpertsCount();
            situationsCount = data.getSituationsCount();
            criteriaCount = data.getCriteriaCount();
            alternativesCount = data.getAlternativesCount();

            //result = new List<Double>(capacity: alternativesCount);
            result = new Dictionary<Int32, Double>(alternativesCount);
            resultFull = new List<Double>(alternativesCount);
            generalPairCompareMatrix = new double[alternativesCount, alternativesCount];
            expertScores = (int[,,,])data.getExpertScores().Clone();
        }

        // Generalized matrix formation for all experts
        private void formGeneralizedMatrix()
        {
            for (int e = 0; e < expertsCount; e++)
            {
                double ke = expertCoefficients[e]; // Коэффициент компетентности эксперта

                for (int s = 0; s < situationsCount; s++)
                {
                    double ks = situationProbabilities[s]; // Вероятность ситуации

                    for (int c = 0; c < criteriaCount; c++)
                    {
                        double kc = criteriaValues[c]; // Коэффициент значимости признака

                        for (int a1 = 0; a1 < alternativesCount; a1++)
                        {
                            for (int a2 = 0; a2 < alternativesCount; a2++)
                            {
                                if (expertScores[e, s, a1, c] <= expertScores[e, s, a2, c])
                                {
                                    generalPairCompareMatrix[a1, a2] += kc * ks * ke;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Generalized matrix formation for one expert
        private void formGeneralizedMatrix(int expertNumber)
        {
            int e = expertNumber;
            double ke = 1; // Коэффициент компетентности эксперта

            for (int s = 0; s < situationsCount; s++)
            {
                double ks = situationProbabilities[s]; // Вероятность ситуации

                for (int c = 0; c < criteriaCount; c++)
                {
                    double kc = criteriaValues[c]; // Коэффициент значимости признака

                    for (int a1 = 0; a1 < alternativesCount; a1++)
                    {
                        for (int a2 = 0; a2 < alternativesCount; a2++)
                        {
                            if (expertScores[e, s, a1, c] <= expertScores[e, s, a2, c])
                            {
                                generalPairCompareMatrix[a1, a2] += kc * ks * ke;
                            }
                        }
                    }
                }
            }
        }

        // Building median matrix: convert generalPairCompareMatrix doubles to 1 and 0 based on
        private void buildMedianMatrix()
        {
            for (int a1 = 0; a1 < alternativesCount; a1++)
            {
                for (int a2 = 0; a2 < alternativesCount; a2++)
                {
                    double t = generalPairCompareMatrix[a1, a2] >= 0.5 ? 1 : 0;
                    generalPairCompareMatrix[a1, a2] = t;
                    sumAlternativesScore += t;
                }
            }
        }

        // Defining the factors of the alternatives preferences
        private void defineFactorsOfAlternatives()
        {
            for (int a1 = 0; a1 < alternativesCount; a1++)
            {
                double rowSum = 0;
                for (int a2 = 0; a2 < alternativesCount; a2++)
                {
                    rowSum += generalPairCompareMatrix[a1, a2];
                }
                result.Add(a1, rowSum / sumAlternativesScore);
                resultFull.Add(rowSum / sumAlternativesScore);
            }
        }

        public List<Double> getFullResult()
        {
            return resultFull;
        }


        public Dictionary<Int32, Int32> getRanks(Dictionary<Int32, Double> result)
        {
            List<Double> values = result.Select(r => r.Value).ToList();
            List<Int32> keys = result.Select(r => r.Key).ToList();

            int rank = 1;
            Dictionary<Int32, Int32> ranks = new Dictionary<Int32, Int32>(result.Count());
            for (int i = 0; i < values.Count(); i++)
            {
                if (i != 0 && values[i - 1] != values[i])
                {
                    rank++;
                }
                ranks.Add(keys[i], rank);
            }

            return ranks;
        }


        // Get the alternatives ranging
        public Dictionary<Int32, Int32> getResult()
        {
            Dictionary<Int32, Double> reversedResult = new Dictionary<Int32, Double>();
            foreach (KeyValuePair<Int32, Double> number in result.OrderBy(key => key.Value).Reverse())
            {
                reversedResult.Add(number.Key, number.Value);
            }
            return getRanks(reversedResult);
        }


        // Process the method for all experts
        public List<Double> processAll()
        {
            sumAlternativesScore = 0;
            //result = new List<Double>(capacity: alternativesCount);
            result = new Dictionary<Int32, Double>(alternativesCount);
            resultFull = new List<Double>(alternativesCount);
            generalPairCompareMatrix = new double[alternativesCount, alternativesCount];
            formGeneralizedMatrix();
            buildMedianMatrix();
            defineFactorsOfAlternatives();
            return getFullResult();
        }

        // Process the method for one experts
        public Dictionary<Int32, Int32> processOneExpert(int i)
        {
            sumAlternativesScore = 0;
            //result = new List<Double>(capacity: alternativesCount);
            result = new Dictionary<Int32, Double>(alternativesCount);
            resultFull = new List<Double>(alternativesCount);
            generalPairCompareMatrix = new double[alternativesCount, alternativesCount];
            formGeneralizedMatrix(i);
            buildMedianMatrix();
            defineFactorsOfAlternatives();
            return getResult();
        }
    }
}