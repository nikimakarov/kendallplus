﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kendall
{
    public class PURrAfterKendall
    {
        private double sumAlternativesScore = 0;

        private List<Double> expertCoefficients;
        private List<Double> situationProbabilities;
        private List<Double> criteriaValues;
        private int[,,,] expertScores;

        private int expertsCount;
        private int situationsCount;
        private int criteriaCount;
        private int alternativesCount;

        private double[,] generalPairCompareMatrix;

        //private List<Double> result;
        private Dictionary<Int32, Double> result;
        private int[,] expertsScores;


        // Input values initialization
        public PURrAfterKendall(MethodsData data, int[,] scores)
        {
            expertCoefficients = new List<Double>(data.getExpertCoefficients());
            situationProbabilities = new List<Double>(data.getSituationProbabilities());
            criteriaValues = new List<Double>(data.getCriteriaValues());

            expertsCount = data.getExpertsCount();
            situationsCount = data.getSituationsCount();
            criteriaCount = data.getCriteriaCount();
            alternativesCount = data.getAlternativesCount();

            //result = new List<Double>(capacity: alternativesCount);
            result = new Dictionary<Int32, Double>(alternativesCount);
            generalPairCompareMatrix = new double[alternativesCount, alternativesCount];
            expertsScores = (int[,])scores.Clone();
        }

        // Generalized matrix formation for all experts
        private void formGeneralizedMatrix()
        {
            for (int e = 0; e < expertsCount; e++)
            {
                double ke = expertCoefficients[e]; // Коэффициент компетентности эксперта
                double ks = 1;
                double kc = 1;
                for (int a1 = 0; a1 < alternativesCount; a1++)
                {
                    for (int a2 = 0; a2 < alternativesCount; a2++)
                    {
                        if (expertsScores[e,a1] <= expertsScores[e,a2])
                        {
                            generalPairCompareMatrix[a1, a2] += kc * ks * ke;
                        }
                    }
                }
            }
        }

        // Building median matrix: convert generalPairCompareMatrix doubles to 1 and 0 based on
        private void buildMedianMatrix()
        {
            for (int a1 = 0; a1 < alternativesCount; a1++)
            {
                for (int a2 = 0; a2 < alternativesCount; a2++)
                {
                    double t = generalPairCompareMatrix[a1, a2] >= 0.5 ? 1 : 0;
                    generalPairCompareMatrix[a1, a2] = t;
                    sumAlternativesScore += t;
                }
            }
        }

        // Defining the factors of the alternatives preferences
        private void defineFactorsOfAlternatives()
        {
            for (int a1 = 0; a1 < alternativesCount; a1++)
            {
                double rowSum = 0;
                for (int a2 = 0; a2 < alternativesCount; a2++)
                {
                    rowSum += generalPairCompareMatrix[a1, a2];
                }
                result.Add(a1, rowSum / sumAlternativesScore);
            }
        }

        // Get the alternatives ranging
        public List<Double> getResult()
        {
            List<Double> positions = new List<Double>();
            foreach (KeyValuePair<Int32, Double> number in result.OrderBy(key => key.Value).Reverse())
            {
                //positions.Add(number.Key + 1);
                positions.Add(number.Value);
            }
            return positions;
        }

        // Process the method for all experts
        public List<Double> processAll()
        {
            sumAlternativesScore = 0;
            //result = new List<Double>(capacity: alternativesCount);
            result = new Dictionary<Int32, Double>(alternativesCount);
            generalPairCompareMatrix = new double[alternativesCount, alternativesCount];
            formGeneralizedMatrix();
            buildMedianMatrix();
            defineFactorsOfAlternatives();
            return getResult();
        }
    }
}