﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kendall
{
    public class MethodsData
    {
        private List<String> expertsNames = new List<String>();
        private List<String> situationsNames = new List<String>();
        private List<String> critertiasNames = new List<String>();
        private List<String> alternativesNames = new List<String>();

        private int expertsCount;
        private int situationsCount;
        private int alternativesCount;
        private int criteriaCount;

        private List<Double> expertCoefficients;
        private List<Double> situationProbabilities;
        private List<Double> criteriaValues;
        private int[,,,] expertScores;

        public MethodsData(List<String> expertsNames, List<String> situationsNames, List<String> critertiasNames, List<String> alternativesNames,
            List<Double> expertCoefficients, List<Double> situationProbabilities, List<Double> criteriaValues)
        {
            this.expertCoefficients = new List<Double>(expertCoefficients);
            this.situationProbabilities = new List<Double>(situationProbabilities);
            this.criteriaValues = new List<Double>(criteriaValues);

            this.expertsNames = new List<String>(expertsNames);
            this.situationsNames = new List<String>(situationsNames);
            this.critertiasNames = new List<String>(critertiasNames);
            this.alternativesNames = new List<String>(alternativesNames);

            expertsCount = this.expertCoefficients.Count();
            situationsCount = this.situationProbabilities.Count();
            criteriaCount = this.criteriaValues.Count();
            alternativesCount = this.alternativesNames.Count();
        }

        public void setExpertScores(int[,,,] expertScores)
        {
            this.expertScores = (int[,,,])expertScores.Clone();
        }

        public List<Double> getExpertCoefficients()
        {
            return expertCoefficients;
        }

        public List<Double> getSituationProbabilities()
        {
            return situationProbabilities;
        }

        public List<Double> getCriteriaValues()
        {
            return criteriaValues;
        }

        public int[,,,] getExpertScores()
        {
            return expertScores;
        }

        public int getExpertsCount()
        {
            return expertsCount;
        }

        public int getSituationsCount()
        {
            return situationsCount;
        }

        public int getAlternativesCount()
        {
            return alternativesCount;
        }

        public int getCriteriaCount()
        {
            return criteriaCount;
        }

        public List<String> getExpertsNames()
        {
            return expertsNames;
        }

        public List<String> getAlternativesNames()
        {
            return alternativesNames;
        }

        public List<String> getSituationsNames()
        {
            return situationsNames;
        }

        public List<String> getCriteriasNames()
        {
            return critertiasNames;
        }
    }
}
